# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Safa Alfulaij <safa1996alfulaij@gmail.com>, 2015, 2018.
# SPDX-FileCopyrightText: 2021, 2022, 2024 Zayed Al-Saidi <zayed.alsaidi@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-11-27 00:40+0000\n"
"PO-Revision-Date: 2024-08-15 10:54+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 23.08.5\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "صفا الفليج"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "safa1996alfulaij@gmail.com"

#: autodetect/widgets/selectprogramlistwidget.cpp:46
#, kde-format
msgid "No program found."
msgstr "لم يُعثر على برامج."

#: importwizard.cpp:41
#, kde-format
msgctxt "@title:window"
msgid "PIM Import Tool"
msgstr "أداة استيراد PIM"

#: importwizard.cpp:70
#, kde-format
msgid ""
"Close KMail before importing data. Some plugins will modify KMail config "
"file."
msgstr "أغلق «بريدك» قبل استيراد البيانات. ستعدّل بعض الملحقات ملفّ ضبط «بريدك»."

#: importwizard.cpp:130
#, kde-format
msgid "Step 1: Select Filter"
msgstr "خطوة 1: اختر مرشّحًا"

#: importwizard.cpp:135
#, kde-format
msgid "Step 2: Importing..."
msgstr "خطوة 2: يستورد..."

#: importwizard.cpp:164
#, kde-format
msgid "Detect program"
msgstr "اكتشف البرنامج"

#: importwizard.cpp:169
#, kde-format
msgid "Select material to import"
msgstr "اختر الموادّ لاستيرادها"

#: importwizard.cpp:173
#, kde-format
msgid "Import mail messages"
msgstr "استورد رسائل البريد"

#: importwizard.cpp:177
#, kde-format
msgid "Import mail filters"
msgstr "استورد مرشّحات البريد"

#. i18n: ectx: property (text), widget (QPushButton, importSettings)
#: importwizard.cpp:181 ui/autodetect/importsettingpage.ui:47
#, kde-format
msgid "Import settings"
msgstr "استورد الإعدادات"

#: importwizard.cpp:185
#, kde-format
msgid "Import addressbooks"
msgstr "استورد دفاتر العناوين"

#: importwizard.cpp:189
#, kde-format
msgid "Import calendars"
msgstr "استورد التّقاويم"

#: importwizard.cpp:193
#, kde-format
msgid "Finish"
msgstr "أنهِ"

#: importwizard.cpp:215
#, kde-format
msgid "Import addressbook from %1..."
msgstr "يستورد دفتر العناوين من %1..."

#: importwizard.cpp:218
#, kde-format
msgid "Import addressbook from %1: Done"
msgstr "يستورد دفتر العناوين من %1: تمّ"

#: importwizard.cpp:224
#, kde-format
msgid "Import filters from %1..."
msgstr "يستورد المرشّحات من %1..."

#: importwizard.cpp:227
#, kde-format
msgid "Import filters from %1: Done"
msgstr "يستورد المرشّحات من %1: تمّ"

#: importwizard.cpp:233
#, kde-format
msgid "Import mails from %1..."
msgstr "يستورد البريد من %1..."

#: importwizard.cpp:236
#, kde-format
msgid "Import mails from %1: Done"
msgstr "يستورد البُرد من %1: تمّ"

#: importwizard.cpp:242
#, kde-format
msgid "Import settings from %1..."
msgstr "يستورد الإعدادات من %1..."

#: importwizard.cpp:245
#, kde-format
msgid "Import settings from %1: Done"
msgstr "يستورد الإعدادات من %1: تمّ"

#: importwizard.cpp:251
#, kde-format
msgid "Import calendar from %1..."
msgstr "يستورد التّقويم من %1..."

#: importwizard.cpp:254
#, kde-format
msgid "Import calendar from %1: Done"
msgstr "يستورد التّقويم من %1: تمّ"

#: importwizard.cpp:343 plugins/balsa/balsaimportdata.cpp:46
#: plugins/claws-mail/clawsmailimportdata.cpp:47
#: plugins/evolutionv3/evolutionv3importdata.cpp:66
#: plugins/icedove/icedoveimportdata.cpp:79
#: plugins/seamonkey/seamonkeyimportdata.cpp:79
#: plugins/sylpheed/sylpheedimportdata.cpp:60
#: plugins/thunderbird/thunderbirdimportdata.cpp:81
#, kde-format
msgid "Import in progress"
msgstr "يجري الاستيراد..."

#: importwizard.cpp:350 plugins/balsa/balsaimportdata.cpp:53
#: plugins/claws-mail/clawsmailimportdata.cpp:55
#: plugins/evolutionv3/evolutionv3importdata.cpp:74
#: plugins/icedove/icedoveimportdata.cpp:87
#: plugins/seamonkey/seamonkeyimportdata.cpp:87
#: plugins/sylpheed/sylpheedimportdata.cpp:68
#: plugins/thunderbird/thunderbirdimportdata.cpp:89
#, kde-format
msgid "Import finished"
msgstr "انتهى الاستيراد"

#: libimportwizard/abstractaddressbook.cpp:28
#, kde-format
msgid "Creating new contact..."
msgstr "ينشئ متراسلًا جديدًا..."

#: libimportwizard/abstractaddressbook.cpp:34
#, kde-format
msgctxt "@title:window"
msgid "Select Address Book"
msgstr "اختيار دفتر عناوين"

#: libimportwizard/abstractaddressbook.cpp:35
#, kde-format
msgid "Select the address book the new contact shall be saved in:"
msgstr "اختر دفتر العناوين لحفظ المتراسلين الجدد فيه:"

#: libimportwizard/abstractaddressbook.cpp:40
#, kde-format
msgid "Address Book was not selected."
msgstr "لم يُحدّد دفتر عناوين."

#: libimportwizard/abstractaddressbook.cpp:67
#, kde-format
msgid "Imported from \"%1\""
msgstr "مستورد من ”%1“"

#: libimportwizard/abstractaddressbook.cpp:86
#, kde-format
msgid "Error during contact creation: %1"
msgstr "خطأ أثناء إنشاء المتراسل: %1"

#: libimportwizard/abstractaddressbook.cpp:89
#, kde-format
msgid "Contact creation complete"
msgstr "اكتمل إنشاء المتراسل"

#: libimportwizard/abstractimporter.cpp:65
#, kde-format
msgid "Importing of filters from \"%1\" was canceled."
msgstr "أُلغي استيراد المرشّحات من ”%1“."

#: libimportwizard/abstractimporter.cpp:67
#, kde-format
msgid "1 filter was imported from \"%2\""
msgid_plural "%1 filters were imported from \"%2\""
msgstr[0] "لم أستورد أيّ مرشّح من ”%2“"
msgstr[1] "استوردتُ مرشّحًا واحدًا من ”%2“"
msgstr[2] "استوردتُ مرشّحين من ”%2“"
msgstr[3] "استوردتُ %1 مرشّحات من ”%2“"
msgstr[4] "استوردتُ %1 مرشّحًا من ”%2“"
msgstr[5] "استوردتُ %1 مرشّح من ”%2“"

#: libimportwizard/abstractimporter.cpp:71
#, kde-format
msgid "Filters file was not found"
msgstr "لم يُعثر على ملفّات المرشّحات"

#: libimportwizard/abstractsettings.cpp:38
#, kde-format
msgid "Setting up identity..."
msgstr "يضبط الهويّة..."

#: libimportwizard/abstractsettings.cpp:46
#, kde-format
msgid "Identity set up."
msgstr "ضُبطت الهويّة."

#: libimportwizard/abstractsettings.cpp:63
#, kde-format
msgid "Setting up transport..."
msgstr "يضبط النّقل..."

#: libimportwizard/abstractsettings.cpp:75
#, kde-format
msgid "Transport set up."
msgstr "ضُبط النّقل."

#: main.cpp:32 main.cpp:34
#, kde-format
msgid "PIM Import Tool"
msgstr "أداة استيراد PIM"

#: main.cpp:36
#, kde-format
msgid "Copyright © 2012-%1 ImportWizard authors"
msgstr "الحقوق محفوظة ©2012-%1 لمؤلّفي «مرشد الاستيراد»"

#: main.cpp:38
#, kde-format
msgctxt "@info:credit"
msgid "Laurent Montel"
msgstr "لوران مونتال"

#: main.cpp:38
#, kde-format
msgid "Maintainer"
msgstr "المصين"

#: main.cpp:45
#, kde-format
msgctxt "@info:shell"
msgid "Mode: %1"
msgstr "الوضع: %1"

#: manual/manualselectfilterpage.cpp:85
#, kde-format
msgid "<p><i>Written by %1.</i></p>"
msgstr "<p><i>كتبه %1.</i></p>"

#: plugins/balsa/balsaaddressbook.cpp:36 plugins/balsa/balsaaddressbook.cpp:49
#: plugins/sylpheed/sylpheedaddressbook.cpp:27
#, kde-format
msgid "No addressbook found"
msgstr "لم يُعثر على دفتر عناوين"

#: plugins/balsa/balsaaddressbook.cpp:63
#, kde-format
msgid "Ldap created"
msgstr "أُنشئ LDAP"

#: plugins/balsa/balsaaddressbook.cpp:93
#, kde-format
msgid "New addressbook created: %1"
msgstr "أُنشئ دفتر عناوين جديد: %1"

#: plugins/claws-mail/clawsmailimportdata.cpp:76
#, kde-format
msgid "Claws Mail settings not found."
msgstr "لم يُعر على إعدادات «بردي كلاوس»."

#: plugins/evolutionv3/evolutionaddressbook.cpp:21
#, kde-format
msgid ""
"Evolution address book will be exported as vCard. Import vCard in "
"KAddressBook."
msgstr "سيُستورد دفتر عناوين «إفلوشن» كَ‍ vCard. استورد vCard في «دفتر عناوينك»."

#: plugins/evolutionv3/evolutionaddressbook.cpp:22
#, kde-format
msgctxt "@title:window"
msgid "Export Evolution Address Book"
msgstr "صدّر دفتر عناوين «إفلوشن»"

#: plugins/evolutionv3/evolutionaddressbook.cpp:25
#, kde-format
msgctxt "@title:window"
msgid "Select the directory where vCards will be stored."
msgstr "اختر الدّليل الذي ستُخزَّن فيه بطاقات vCard."

#: plugins/evolutionv3/evolutionaddressbook.cpp:78
#, kde-format
msgid "Address book \"%1\" exported."
msgstr "صُدّر دفتر العناوين ”%1“."

#: plugins/evolutionv3/evolutionaddressbook.cpp:80
#, kde-format
msgid "Failed to export address book \"%1\"."
msgstr "فشل تصدير دفتر العناوين ”%1“."

#: plugins/evolutionv3/evolutionv3importdata.cpp:57
#, kde-format
msgid "Evolution settings not found."
msgstr "لم يُعثر على إعدادات «إفلوشن»."

#: plugins/evolutionv3/evolutionv3importdata.cpp:100
#, kde-format
msgid "Evolution calendar not found."
msgstr "لم يُعثر على تقويم «إفلوشن»."

#: plugins/icedove/icedoveimportdata.cpp:69
#, kde-format
msgid "Icedove settings not found."
msgstr "لم يُعثر على إعدادات «آيس‌دوف»."

#: plugins/seamonkey/seamonkeyimportdata.cpp:69
#, kde-format
msgid "SeaMonkey settings not found."
msgstr "لم يُعثر على إعدادات «سي‌مَنكي»."

#: plugins/sylpheed/sylpheedaddressbook.cpp:60
#, kde-format
msgid "No contact found in %1"
msgstr "لم يُعثر على متراسلين في %1"

#: plugins/sylpheed/sylpheedimportdata.cpp:51
#, kde-format
msgid "Sylpheed settings not found."
msgstr "لم يُعثر على إعدادات Sylpheed."

#: plugins/thunderbird/thunderbirdaddressbook.cpp:46
#, kde-format
msgid "Contacts file '%1' not found"
msgstr "لم يُعثر على ملفّات المتراسلين ’%1‘"

#: plugins/thunderbird/thunderbirdimportdata.cpp:71
#, kde-format
msgid "Thunderbird settings not found."
msgstr "لم يُعثر على إعدادات «ثَندربيرد»."

#. i18n: ectx: property (text), widget (QPushButton, importAddressBook)
#: ui/autodetect/importaddressbookpage.ui:47
#, kde-format
msgid "Import addressbook"
msgstr "استورد دفتر عناوين"

#. i18n: ectx: property (text), widget (QPushButton, importCalendar)
#: ui/autodetect/importcalendarpage.ui:47
#, kde-format
msgid "Import calendar"
msgstr "استورد تقويمًا"

#. i18n: ectx: property (text), widget (QPushButton, importFilters)
#: ui/autodetect/importfilterpage.ui:47
#, kde-format
msgid "Import Filters"
msgstr "استورد مرشّحات"

#. i18n: ectx: property (text), widget (QLabel, label)
#: ui/autodetect/importfinishpage.ui:29
#, kde-format
msgid "Summary:"
msgstr "الملخّص:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: ui/autodetect/importmailpage.ui:32 ui/manual/manualselectfilterpage.ui:136
#, kde-format
msgid "Please select the folder to import to:"
msgstr "رجاءً اختر المجلّد للاستيراد فيه:"

#. i18n: ectx: property (text), widget (QPushButton, importMails)
#: ui/autodetect/importmailpage.ui:60
#, kde-format
msgid "Import Mails"
msgstr "استورد الرّسائل"

#. i18n: ectx: property (text), widget (QCheckBox, mails)
#: ui/autodetect/selectcomponentpage.ui:49
#, kde-format
msgid "Mails"
msgstr "رسائل البريد"

#. i18n: ectx: property (text), widget (QCheckBox, filters)
#: ui/autodetect/selectcomponentpage.ui:59
#, kde-format
msgid "Filters"
msgstr "المرشّحات"

#. i18n: ectx: property (text), widget (QCheckBox, settings)
#: ui/autodetect/selectcomponentpage.ui:69
#, kde-format
msgid "Settings"
msgstr "الإعدادات"

#. i18n: ectx: property (text), widget (QCheckBox, addressbooks)
#: ui/autodetect/selectcomponentpage.ui:79
#, kde-format
msgid "Address Books"
msgstr "دفاتر العناوين"

#. i18n: ectx: property (text), widget (QCheckBox, calendars)
#: ui/autodetect/selectcomponentpage.ui:89
#, kde-format
msgid "Calendars"
msgstr "التّقاويم"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: ui/autodetect/selectcomponentpage.ui:126
#, kde-format
msgid ""
"This wizard will import mail messages, addressbook entries, filters and/or "
"settings from other mail programs into KMail"
msgstr ""
"سيستورد هذا المرشد رسائل البريد، ومدخلات دفاتر العناوين، والمرشّحات و/أو "
"الإعدادات من برامج البريد الأخرى إلى «بريدك»"

#. i18n: ectx: property (text), widget (QLabel, label)
#: ui/autodetect/selectcomponentpage.ui:136
#, kde-format
msgid "Or select types of material to import:"
msgstr "أو حدّد أنواع الموادّ لاستيرادها:"

#. i18n: ectx: property (text), widget (QCheckBox, everything)
#: ui/autodetect/selectcomponentpage.ui:143
#, kde-format
msgid "Import Everything"
msgstr "استورد كلّ شيء"

#. i18n: ectx: property (text), widget (QLabel, label)
#: ui/autodetect/selectprogrampage.ui:53
#, kde-format
msgid "Please select the program from which you like to import:"
msgstr "رجاءً اختر البرنامج الذي تريد الاستيراد منه:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: ui/autodetect/selectprogrampage.ui:86
#, kde-format
msgid "No program found"
msgstr "لم يُعثر على برامج"

#. i18n: ectx: property (text), widget (QCheckBox, manualSelectionCheckBox)
#: ui/autodetect/selectprogrampage.ui:112
#, kde-format
msgid "Manual Selection"
msgstr "تحديد يدويّ"

#. i18n: ectx: property (text), widget (QLabel, textLabel1_2)
#: ui/manual/manualimportmailpage.ui:96
#, kde-format
msgid "Click 'Back' to import more emails"
msgstr "انقر ’عُد‘ لاستيراد رسائل أخرى"

#. i18n: ectx: property (text), widget (QLabel, TextLabel6)
#: ui/manual/manualselectfilterpage.ui:103
#, kde-format
msgid ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:10pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"family:'DejaVu Sans'; font-size:9pt; font-weight:600;\">Welcome to "
"ImportWizard The Mail Import Tool</span><span style=\" font-family:'DejaVu "
"Sans'; font-size:9pt;\"> <br /><br />This program will help you import your "
"email from your previous email program.<br /><br />Please select the program "
"you would like to import from. Next, select the folder you would like to "
"import to, then click 'Next'.</span></p></body></html>"
msgstr ""
"<b>مرحبًا في «مرشد الاستيراد»، أداة استيراد البريد</b><br /><p>سيساعدك هذا "
"البرنامج لاستيراد البريد الإلكترونيّ من برامج البريد الأخرى.<br /><br />رجاءً "
"اختر البرنامج الذي تريد الاستيراد منه، وبعدها اختر المجلّد الذي تريد "
"الاستيراد إليه، وثمّ انقر ’التّالي‘."

#. i18n: ectx: property (text), widget (QCheckBox, remDupMsg)
#: ui/manual/manualselectfilterpage.ui:146
#, kde-format
msgid "Remove &duplicate messages during import"
msgstr "أزِل الرّسائل المت&كرّرة أثناء الاستيراد"
